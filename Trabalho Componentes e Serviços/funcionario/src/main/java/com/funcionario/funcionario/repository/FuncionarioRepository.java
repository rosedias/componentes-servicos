package com.funcionario.funcionario.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.funcionario.funcionario.models.Funcionario;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long>{
	
	Funcionario findById(long id);

}