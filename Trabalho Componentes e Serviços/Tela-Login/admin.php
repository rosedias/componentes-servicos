<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Admin</title>
        <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
        <style>
            body {
                margin: 50px 0 0 0;
                padding: 0;
                width: 100%;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                text-align: center;
                color: #aaa;
                font-size: 18px;
            }

            h1 {
                color: #719e40;
                letter-spacing: -3px;
                font-family: 'Lato', sans-serif;
                font-size: 100px;
                font-weight: 200;
                margin-bottom: 0;
            }

            table, form {
              width: 100%;
              max-width: 600px;
              margin: 40px auto;
            }
            form input, .btn {
              display: block;
              padding: 20px;
              border-radius: 10px;
              border: 1px solid #ccc;
              width: 100%;
              margin: 10px 0;
            }
            form input[type='submit'], .btn {
              cursor: pointer;
              color: #fff;
              background-color: #719e40;
            }
            
        </style>
    </head>
    <body>
        <h1>PetShop - Tela de Admin</h1>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Nome</th>
              <th>Nível Acesso</th>
              <td><a href="http://localhost:8000/admin" class="btn" method="get">buscar</a></td>
            </tr>
          </thead>      
        </table>

      
    </body>
</html>